import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subiect2 extends JFrame {

    JButton button;
    JTextField text1;
    JTextField text2;
    int flag = 0;
    String string = "Exemplu Text";

    Subiect2() {

        init();
        button();
    }

    public void init() {

        this.setLayout(null);
        setTitle("Problema Subiect2");
        setSize(500,300);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        text1 = new JTextField();
        text1.setBounds(10,50,200,20);

        text2 = new JTextField();
        text2.setBounds(10,80,200,20);

        add(text1);add(text2);
    }

    public void button() {

        button = new JButton("Swap Text");
        button.setBounds(10,10,100,20);
        add(button);
        button.addActionListener(new Subiect2.buttonPress());

    }

    class buttonPress implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            if (flag == 0) {
                text1.setText(string);
                text2.setText("");
                flag = 1;
            }
            else
            {
                text2.setText(text1.getText());
                text1.setText("");
                flag = 0;
            }
        }
    }

    public static void main(String[] args) {
        new Subiect2();
    }
}